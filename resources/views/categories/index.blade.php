@extends('layouts.app')

@section('content')
    <h1>Product categories</h1>

    <hr/>

    <div class="form-group">
        <a href="/dashboard" class="btn btn-success">Back</a>
    </div>

    <div class="well well-lg text-center">
        {!! Form::open(['method' => 'POST', 'route' => 'categories.store']) !!}
        @include('categories._form', ['submitButtonText' => 'Add Category'])
        {!! Form::close() !!}

        @include('errors.list')
    </div>

    <div class="well well-lg">
        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td>
                        {!! Form::model($category, ['method' => 'PUT', 'route' => ['categories.update', $category->id]]) !!}
                        @include('categories._form', ['submitButtonText' => 'Save'])
                        {!! Form::close() !!}
                    </td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['categories.destroy', $category->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop