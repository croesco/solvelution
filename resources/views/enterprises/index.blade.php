@extends('layouts.app')

@section('content')
    <h1>Enterprises</h1>

    <hr/>

    <div class="form-group">
        <a href="/dashboard" class="btn btn-success">Back</a>
    </div>

    <div class="well well-lg text-center">
        {!! Form::open(['method' => 'POST', 'route' => 'enterprises.store']) !!}
        <div class="row">
            <div class="col-xs-6">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'Enterprise name']) !!}
            </div>
            <div class="col-xs-6">
                {!! Form::submit('Add Enterprise', ['class' => 'btn btn-primary form-control']) !!}
            </div>
        </div>
        {!! Form::close() !!}

        @include('errors.list')
    </div>

    <div class="well well-lg">
        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($enterprises as $enterprise)
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-xs-6">
                                {{$enterprise->name}}
                            </div>
                            <div class="col-xs-6">
                                <a href="/enterprises/{{$enterprise->id}}/edit"
                                   class="btn btn-success form-control">Edit</a>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop