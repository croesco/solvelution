@extends('layouts.app')

@section('content')
    <h1>Products</h1>

    <hr/>

    <div class="form-group">
        <a href="/dashboard" class="btn btn-success">Back</a>
    </div>

    <div class="well well-lg text-center">
        <a href="/products/create" class="btn btn-primary form-control">Add new product</a>
    </div>

    <div class="well well-lg">
        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($products as $product)
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-xs-3">
                                {{$product->name}}
                            </div>
                            <div class="col-xs-3">
                                {{$product->category->name}}
                            </div>
                            <div class="col-xs-3">
                                {{$product->enterprise->name}}
                            </div>
                            <div class="col-xs-3">
                                <a href="/products/{{$product->id}}/edit"
                                   class="btn btn-success form-control">Edit</a>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop