@extends('layouts.app')

@section('content')
    <h1>New Product</h1>

    <hr/>

    <div class="form-group">
        <a href="/products" class="btn btn-success">Back</a>
    </div>

    <div class="well well-lg text-center">
        {!! Form::open(['method' => 'POST', 'route' => 'products.store']) !!}
        @include('products._form', ['submitButtonText' => 'Create'])
        {!! Form::close() !!}

        @include('errors.list')
    </div>
@stop