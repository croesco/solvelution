<div class="row form-group">
    <div class="col-xs-4">
        {!! Form::label('name', 'Product name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'Product name']) !!}
    </div>
    <div class="col-xs-3">
        {!! Form::label('category_id', 'Category:') !!}
        {!! Form::select('category_id', $categoriesArr, null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-xs-3">
        {!! Form::label('enterprise_id', 'Enterprise:') !!}
        {!! Form::select('enterprise_id', $enterprisesArr, null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-xs-2">
        {!! Form::label('price', 'Price:') !!}
        {!! Form::text('price',  null, ['class' => 'form-control', 'placeholder'=>'Price']) !!}
    </div>
</div>
<div class="row form-group">
    <div class="col-xs-12">
        {!! Form::label('origin_link', 'Origin link:') !!}
        {!! Form::text('origin_link',  null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="row form-group">
    <div class="col-xs-12">
        {!! Form::label('description', 'Description:') !!}
        {!! Form::textarea('description', null,
        ['size' => '30x4', 'class' => 'form-control', 'placeholder'=>'Description']) !!}
    </div>
</div>
<div class="row">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
</div>
