<?php

namespace App\Http\Controllers;

use App\Category;
use App\Enterprise;
use App\Http\Requests\ProductRequest;
use App\Product;
use Request;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::get();

        return view('products.index', compact('products'));
    }

    public function create()
    {
        $categoriesArr = Category::pluck('name', 'id');
        $enterprisesArr = Enterprise::pluck('name', 'id');

        return view('products.create', compact('categoriesArr', 'enterprisesArr'));
    }

    public function edit($product)
    {
        $categoriesArr = Category::pluck('name', 'id');
        $enterprisesArr = Enterprise::pluck('name', 'id');

        return view('products.edit', compact('product', 'categoriesArr', 'enterprisesArr'));
    }

    public function store(ProductRequest $request)
    {
        $product = new Product($request->all());

        $product->save();

        return redirect('products');
    }

    public function update($product, ProductRequest $request)
    {
        $product->update($request->all());

        return redirect('products');
    }

    public function destroy($product)
    {
        $product->delete();

        return redirect('products');
    }
}
