<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CategoryRequest;
use Request;

class CategoriesController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function index()
    {
        $categories = Category::get();

        return view('categories.index', compact('categories'));
    }

    public function store(CategoryRequest $request)
    {
        $category = new Category($request->all());
        $category->save();

        return redirect('categories');
    }

    public function update($category, CategoryRequest $request)
    {
        $category->update($request->all());

        return redirect('categories');
    }

    public function destroy($category)
    {
        $category->delete();

        return redirect('categories');
    }
}
