<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Auth\RegisterController;
use Closure;
use Illuminate\Support\Facades\Auth;

class RegisterAnonymously
{
    /**
     * The guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $regController;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory $auth
     * @return void
     */
    public function __construct(RegisterController $regController)
    {
        $this->regController = $regController;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::guard($guard)->check()) {
            $this->regController->registerAnonymous();
        }

        return $next($request);
    }
}
