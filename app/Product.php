<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'category_id',
        'enterprise_id',
        'name',
        'description',
        'price',
        'origin_link'
    ];

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function enterprise() {
        return $this->belongsTo(Enterprise::class);
    }
}