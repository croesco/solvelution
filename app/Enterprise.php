<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enterprise extends Model
{
    protected $fillable = [
        'name',
        'description',
        'integration_module'
    ];

    public function products() {
        return $this->hasMany(Product::class);
    }
}
